/*jslint browser: true*/
/*jslint white: true*/
/*jshint strict: true*/
/*global $, jQuery, alert*/

//function for hiding the results table
window.onload = function () {
    document.getElementById("resultsTable").style.display = "none";
    document.getElementById("playAgain").style.display = "none";
    alert("How many dollars do you have to bet? Please enter that amount into the 'Starting Bet' field.");
};
//function for showing the results table
var showResult = function () {
    document.getElementById("resultsTable").style.display = "block";
    document.getElementById("playAgain").style.display = "block";
};
//Play again or not functions
var PlayAgain = function () {
    location.reload();
};

var Play = function () {
    document.getElementById("play").style.display = "none";
    //get value of starting bet from user input
    var startingBet = document.getElementById("startingBet").value;
    var cash = startingBet;
    var rolls = 0;
    var maxAmountHeld = cash;
    var rollsAtMax = 0;

    while (cash > 0) {
        var dice1 = Math.floor(Math.random() * 7);
        var dice2 = Math.floor(Math.random() * 7);
        var roll = dice1 + dice2;
        rolls++;
        if (roll === 7) {
            cash += 4;
        } else {
            cash -= 1;
        }
        if (cash > maxAmountHeld) {
            maxAmountHeld = cash;
            rollsAtMax = rolls;
        }
        //Populate Table
        document.getElementById("bet").innerHTML = startingBet;
        document.getElementById("tRolls").innerHTML = rolls;
        document.getElementById("highestAmount").innerHTML = maxAmountHeld;
        document.getElementById("rollsHighest").innerHTML = rollsAtMax;
        showResult();
    }
};